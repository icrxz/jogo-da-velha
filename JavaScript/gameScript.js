var gameBoard;

class Player {
    constructor(name, symbol, img) {
        this.name = name;
        this.symbol = symbol;
        this.img = img;
        this.points = 0;
        this.yourTurn = false;
    }
}

class Game {
    constructor(player1, player2) {
        this.Board = [
            ["", "", ""],
            ["", "", ""],
            ["", "", ""]
        ];
        this.rounds = 0;
        this.Player1 = player1;
        this.Player2 = player2;
    }

    insertPlay(element) {
        var pos = String(element.id).replace("btnRow", "").replace("Col", "");
        var posX = pos[0];
        var posY = pos[1];
        if (this.Board[posX][posY] != "") {
            alert("Posição já está marcada!");
            return;
        } else {
            if (this.Player1.yourTurn) {
                this.Board[posX][posY] = this.Player1.symbol;
                $(element).children().text(this.Player1.symbol);
                $(element).css("background-color", "lightgreen");
                $("#player1Turn").text("Player 1");
                $("#player2Turn").text("Player 2, é sua vez");
            } else {
                this.Board[posX][posY] = this.Player2.symbol;
                $(element).children().text(this.Player2.symbol);
                $(element).css("background-color", "lightcoral");
                $("#player2Turn").text("Player 2");
                $("#player1Turn").text("Player 1, é sua vez");
            }
        }
        $("#roundsId").text(++this.rounds);
        this.Player1.yourTurn = this.Player2.yourTurn;
        this.Player2.yourTurn = !this.Player1.yourTurn;
        verifyGame();
    }

    checkBoard() {
        for (let i = 0; i < this.Board.length; i++) {
            if (this.Board[0][i] == this.Board[1][i] && this.Board[1][i] == this.Board[2][i] && this.Board[0][i] != "")
                return true;
            else if (this.Board[i][0] == this.Board[i][1] && this.Board[i][1] == this.Board[i][2] && this.Board[i][0] != "")
                return true;
        }
    }
}

function startGame() {
    var name1;
    if ($('#namePlayer1').val().length > 0)
        name1 = $('#namePlayer1').val();
    else
        name1 = "Jon Snow";
    var pathImg1 = $('#imagePl1').attr('src');
    var name2;
    if ($('#namePlayer2').val().length > 0)
        name2 = $('#namePlayer2').val();
    else
        name2 = "Daenerys Targaryen"
    var pathImg2 = $('#imagePl2').attr('src');
    pl1 = new Player(name1, 'X', pathImg1);
    pl2 = new Player(name2, '0', pathImg2);
    pl1.yourTurn = true;

    gameBoard = new Game(pl1, pl2);

    $('#playerSelection').hide();
    $('#tbGameBoard').show();
    $('#playerInformation').show();
    $('#pageFooter').show();
    generateTable();

    $("#tbGameBoard .btn").click(function() {
        gameBoard.insertPlay(this);
    });

    $('#NameP1').text(gameBoard.Player1.name);
    $('#NameP2').text(gameBoard.Player2.name);
    $('#imagePlayer1').attr('src', gameBoard.Player1.img);
    $('#imagePlayer2').attr('src', gameBoard.Player2.img);
    $("#Points1").val(gameBoard.Player1.points);
    $("#Points2").val(gameBoard.Player2.points);
}

function verifyGame() {
    if (gameBoard.rounds >= 3) {
        if (
            gameBoard.Board[0][0] === gameBoard.Board[1][1] && gameBoard.Board[1][1] === gameBoard.Board[2][2] && gameBoard.Board[2][2] != "" ||
            gameBoard.Board[0][2] === gameBoard.Board[1][1] && gameBoard.Board[1][1] === gameBoard.Board[2][0] && gameBoard.Board[2][0] != "" ||
            gameBoard.checkBoard()
        ) {
            if (!gameBoard.Player1.yourTurn) {
                alert('O jogador ' + gameBoard.Player1.name + ' venceu!');
                $("#Points1").val(++gameBoard.Player1.points);
            } else {
                alert('O jogador ' + gameBoard.Player2.name + ' venceu!');
                $("#Points2").val(++gameBoard.Player2.points);
            }
            restartGame()
        } else if (gameBoard.rounds >= 9) {
            alert("Jogo empatado, a partida irá reiniciar!");
            restartGame();
            return;
        }
    }
}

function restartGame() {
    gameBoard.Board = [
        ["", "", ""],
        ["", "", ""],
        ["", "", ""]
    ];
    gameBoard.rounds = 0;
    $("#roundsId").text(gameBoard.rounds);
    $("#tbGameBoard span").text("");
    $("#tbGameBoard .btnSymb").css("background", "#808080")
}

$().ready(function() {
    $('#tbGameBoard').hide();
    $('#playerInformation').hide();
    $('#pageFooter').hide();

    $("#btnStart").click(function() {
        startGame();
    })

    $("#linkReset").click(function() {
        restartGame();
    });
})

function generateTable() {
    for (let i = 0; i < 3; i++) {
        $("#tbGameBoard tbody").append('<tr id="tbRow' + i + '">');
        for (let j = 0; j < 3; j++) {
            $("#tbRow" + i).append('<td><button class="btn btn-secondary btn-block btnSymb" id="btnRow' + i + 'Col' + j + '"><span ' +
                'style="font-size: 4vw;" id="lbl' + i + j + '"></span></button></td>');
        }
        $("tbGameBoard tbody").append('</tr>');
    }
}